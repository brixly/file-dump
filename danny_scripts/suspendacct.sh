#!/bin/bash

echo -n -e "Please enter the domain name you wish to suspend\n"
read domain
echo -n -e "Please enter the reason the domain should be suspended\n"
read reason

/scripts/suspendacct $(/scripts/whoowns $domain) "$reason" 1