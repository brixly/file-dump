#!/bin/bash

if systemctl is-active --quiet imunify-realtime-av; then
    echo "0 Imunify_Realtime_Scan - running"
else
    echo "2 Imunify_Realtime_Scan - down"
fi
