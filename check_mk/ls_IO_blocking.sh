#!/bin/bash

DEAD_LOOP_PID=$(tac /var/log/apache2/error_log | grep -m1 'possible disk I/O blocking' | cut -d' ' -f8 | tr -d ,)

if [ -z "$DEAD_LOOP_PID" ]; then
    echo "0 LITESPEED_BLOCKING - I/O BlOCKING NOT DETECTED"
elif [[ $(ps aux | grep $DEAD_LOOP_PID | awk '$11 == "litespeed" {print $11}') ]]; then
    echo "2 LITESPEED_BLOCKING - I/O BLOCKING DETECTED, PID $DEAD_LOOP_PID"
else
    echo "0 LITESPEED_BLOCKING - I/O BLOCKING NOT DETECTED"
fi