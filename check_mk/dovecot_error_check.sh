#!/bin/bash

if ! tail -n1000 /var/log/exim_mainlog | grep -q "condition check lookup defer"; then
    echo "0 Dovecot_quota_exceeded - No  recent errors detected"
    exit 0
fi

email_account=$(tac /var/log/maillog | grep -m1 "Mailbox INBOX: file_create_locked" | cut -d\( -f2 | cut -d\) -f1)

local_part=$(echo "$email_account" | cut -d\@ -f1)
domain_part=$(echo "$email_account" | cut -d\@ -f2)
owner=$(/scripts/whoowns "$domain_part")

if [[ "$domain_part" == $(hostname) ]]; then
    quota="unlimited"
    used="somevalue"
    owner="$local_part"
else
    # Ugly way of doing things to cast from float to int
    used=$(uapi --user="$owner" Email get_disk_usage user="$local_part" domain="$domain_part" --output=json | jq -r ".result | .data | .diskused" | cut -d. -f1)
    quota=$(uapi --user="$owner" Email get_pop_quota email="$local_part" domain="$domain_part" --output=json | jq -r ".result | .data" | cut -d. -f1)
fi

if [[ "$used" -gt "$quota" ]] && [[ "$quota" != "unlimited" ]]; then
    echo "2 Dovecot_quota_exceeded - $email_account is over quota and causing Dovecot errors"
elif [ $(uapi --user="$owner" StatsBar get_stats display=diskusage --output=json | jq ".result.data[]._maxed") == 1 ]; then
    echo "2 Dovecot_quota_exceeded - $owner is over quota and causing Dovecot errors"
else
    echo "0 Dovecot_quota_exceeded - Errors detected but resolved"
fi
