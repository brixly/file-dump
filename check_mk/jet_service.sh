#!/bin/bash

if systemctl is-active --quiet jetbackup5d; then
    echo "0 JetBackup5d - running"
else
    echo "2 JetBackup5d - down"
fi