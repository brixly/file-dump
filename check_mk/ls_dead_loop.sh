#!/bin/bash

DEAD_LOOP_PID=$(tac /var/log/apache2/error_log | grep -m1 "dead loop" | cut -d' ' -f8 | tr -d ,)

if [ -z "$DEAD_LOOP_PID" ]; then
    echo "0 LITESPEED_STATUS - DEAD LOOP NOT DETECTED"
elif [[ $(ps aux | grep $DEAD_LOOP_PID | awk '$11 == "litespeed" {print $11}') ]]; then
    echo "2 LITESPEED_STATUS - DEAD LOOP DETECTED, PID $DEAD_LOOP_PID"
else
    echo "0 LITESPEED_STATUS - DEAD LOOP NOT DETECTED"
fi