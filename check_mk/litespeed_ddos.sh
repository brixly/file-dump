#!/bin/bash

BEING_ATTACKED=$(cat /tmp/lshttpd/.rtreport | grep APVH | grep "^REQ_RATE" | tr -d ',' |  awk '$6 > 3000 {print $2}' | sort -rn | head -n1)

if [ ! -z $BEING_ATTACKED ]; then
    echo "2 BEING_ATTACKED - More than 3k active connections to $(echo $BEING_ATTACKED | cut -d_ -f2 | cut -d: -f1)"
else
    echo "0 BEING_ATTACKED - No sites with more than 3k connections"
fi

