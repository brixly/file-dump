#!/bin/bash

ACTIVEUSERLIST="/root/active.services"
rm -f /root/suspicious.all

for user in $(ls /var/cpanel/users)
do
owner=$(cat /var/cpanel/users/${user} |grep "OWNER="|cut -d"=" -f2)
if [ ${owner} == root ]
then
# owner is root(shared),so we crosscheck user
grep -w ${user} ${ACTIVEUSERLIST} || echo ${user} >> /root/suspicious.all
else
# owner is a reseller,so check if reseller is active
grep -w ${owner} ${ACTIVEUSERLIST} || echo ${user} >> /root/suspicious.all
fi
done
