#!/bin/bash

if [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
	echo "Run in the directory of a WordPress site to automatically download a new set of files"
	exit
fi

if test -f "./wp-config.php"; then
    OWNER=$(stat -c '%U' "./wp-config.php")
	PHP_VERSION=$(selectorctl --user-current --user=$OWNER | awk '{print $1}')
	selectorctl --enable-user-extensions=zip --version=$PHP_VERSION --user=$OWNER
	VERSION=$(su -s /bin/bash - $OWNER -c "wp core version --path=$(pwd)")
	su -s /bin/bash - $OWNER -c "wp core download --skip-content --force --version=$VERSION --path=$(pwd)"
	read -p "Search for extra files? (y/n) " extra
	if [ $extra == "y" ]; then
		su -s /bin/bash - $OWNER -c "wp core verify-checksums --version=$VERSION --path=$(pwd) 2>&1 | cut -d: -f3"
	fi
else
	echo "wp-config.php not found!"
fi
