#/bin/bash
rm -f /var/cpanel/hostname_history.json
echo $(curl ifconfig.io) > /var/cpanel/mainip
sed -i "/ADDR /c\ADDR $(curl ifconfig.io)" /etc/wwwacct.conf
readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"
readonly HOSTNAME=$(hostname --fqdn)
readonly CPHULKPASS=$(/usr/local/cpanel/3rdparty/bin/perl -MCpanel::PasswdStrength::Generate -e 'print Cpanel::PasswdStrength::Generate::generate_password(14)')
readonly MSECPASS=$(/usr/local/cpanel/3rdparty/bin/perl -MCpanel::PasswdStrength::Generate -e 'print Cpanel::PasswdStrength::Generate::generate_password(14)')
readonly ESTATSPASS=$(/usr/local/cpanel/3rdparty/bin/perl -MCpanel::PasswdStrength::Generate -e 'print Cpanel::PasswdStrength::Generate::generate_password(14)')
readonly LPROTPASS=$(/usr/local/cpanel/3rdparty/bin/perl -MCpanel::PasswdStrength::Generate -e 'print Cpanel::PasswdStrength::Generate::generate_password(14)')
readonly RCUBEPASS=$(/usr/local/cpanel/3rdparty/bin/perl -MCpanel::PasswdStrength::Generate -e 'print Cpanel::PasswdStrength::Generate::generate_password(14)')
readonly MYSQLPASS=$(/usr/local/cpanel/3rdparty/bin/perl -MCpanel::PasswdStrength::Generate -e 'print Cpanel::PasswdStrength::Generate::generate_password(14)')

is_file() {
    local file=$1
    [[ -f $file  ]]
}

is_dir() {
    local dir=$1
    [[ -d $dir  ]]
}

main() {
    is_dir /usr/local/cpanel \
        && /usr/local/cpanel/bin/set_hostname ${HOSTNAME} \
        && /usr/local/cpanel/bin/checkallsslcerts --allow-retry \
        && /scripts/build_cpnat \
        && /scripts/rebuildhttpdconf \
        && /scripts/mysqlpasswd root ${MYSQLPASS} \
    is_file /var/cpanel/hulkd/password \
        && /scripts/mysqlpasswd cphulkd ${CPHULKPASS} \
        && echo -e "user=\"cphulkd\"\npass=\"${CPHULKPASS}\"">/var/cpanel/hulkd/password \
        && /scripts/restartsrv_cphulkd
    is_file /var/cpanel/modsec_db_pass \
        && /scripts/mysqlpasswd modsec ${MSECPASS} \
        && echo ${MSECPASS} >/var/cpanel/modsec_db_pass
    is_file /var/cpanel/roundcubepass \
        && /scripts/mysqlpasswd roundcube ${RCUBEPASS} \
        && echo ${RCUBEPASS} >/var/cpanel/roundcubepass
    is_file /var/cpanel/eximstatspass \
        && /scripts/mysqlpasswd eximstats ${ESTATSPASS} \
        && echo ${ESTATSPASS} >/var/cpanel/eximstatspass \
        && /scripts/restartsrv_eximstats
    is_file /var/cpanel/leechprotectpass \
        && /scripts/mysqlpasswd leechprotect ${LPROTPASS} \
        && echo ${LPROTPASS} >/var/cpanel/leechprotectpass
}

main

/usr/local/cpanel/bin/checkallsslcerts
/usr/local/cpanel/3rdparty/bin/php /usr/local/cpanel/whostmgr/docroot/cgi/softaculous/cron.php

# Install and Enable Monitoring Site

# Create Account

pass=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c12)

whmapi1 createacct username=brixlymonitoring domain=brixlymonitoring.$(hostname -s).mysitepreview.co.uk maxaddon=unlimited password=$pass

# Install WordPress
/usr/local/cpanel/3rdparty/bin/php /usr/local/cpanel/whostmgr/docroot/cgi/softaculous/cli.php --install --panel_user=brixlymonitoring --panel_pass=ccJOY1uxqhMsdJ86kn5 --soft=26 --admin_username=admin --admin_pass=$pass

# Alternative if above doesn’t work
/usr/local/cpanel/bin/autossl_check --user brixlymonitoring

#Install nixstats

wget -q -N --no-check-certificate https://nixstats.com/nixstatsagent.sh && bash nixstatsagent.sh 5c9a2cdf3f0bc1367b293c79 DVRTRj9DCDDDb7ud1UMamAiz

# nixstats - Exim Monitoring

echo "nixstats ALL=(ALL) NOPASSWD: /usr/sbin/exim" >> /etc/sudoers

service rsyslog restart

# nixstats - Reset nixstats.ini
wget -O /etc/nixstats.ini https://gitlab.com/brixly/file-dump/raw/master/nixstats.ini
service nixstatsagent restart

/usr/local/lsws/admin/misc/lscmctl --update-lib

imunify360-agent register $(cat /etc/i360.key)

imunify360-agent whitelist ip add $(curl ifconfig.io) --scope group --comment="Local Server"
