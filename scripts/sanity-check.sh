#!/bin/bash

RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

pass() {
    test=$1
    echo -e "[${GREEN}Y${ENDCOLOR}] $test"
}

fail() {
    test=$1
    echo -e "[${RED}N${ENDCOLOR}] $test"
}

check_binary_exists() {
    BINARY=$1

    if which $BINARY >/dev/null 2>&1; then
        pass "$BINARY"
    else
        fail "$BINARY"
    fi
}

check_service_exists() {
    SERVICE=$1

    if systemctl is-active --quiet $SERVICE; then
        pass "$SERVICE"
    else
        fail "$SERVICE"
    fi
}

echo -e "###### Kernel ######"
if [[ $(uname -r) =~ ".lve." ]]; then
    pass "LVE Kernel"
else
    fail "No LVE Kernel"
fi
uname -r

echo -e "\n##### Grub #####"
diskpart=$(mount | awk '/\/boot / {print $1}' | cut -d'/' -f3)
if [[ $diskpart =~ "md" ]]; then
    diskpart=$(grep $diskpart /proc/mdstat | awk '{print $5}')
fi
if [[ $diskpart =~ "nvme" ]]; then
    disk=$(echo $diskpart | grep -oE "nvme[0-9]n[0-9]")
else
    disk=$(echo $diskpart | sed 's/[0-9]//g')
fi
if [[ $(dd bs=512 count=1 if=/dev/$disk 2>/dev/null | strings) =~ "GRUB" ]]; then
    pass "GRUB Installed"
else
    fail "Grub not detected"
fi
echo "Disk checked was $disk"

echo -e "\n##### Set Timezone #####"
timedatectl show | awk -F'=' '/Timezone/ {print $2}'

echo -e "\n###### Binaries ######"
check_binary_exists tmux
check_binary_exists ncdu
check_binary_exists imunify360-agent
check_binary_exists cagefsctl
check_binary_exists selectorctl
check_binary_exists mysql
check_binary_exists vim
check_binary_exists python3
check_binary_exists htop
check_binary_exists tcpdump
check_binary_exists nethogs

echo -e "\n##### Services #####"
check_service_exists sshd
check_service_exists imunify360
check_service_exists check_mk.socket
check_service_exists db_governor
check_service_exists lshttpd
check_service_exists mariadb
check_service_exists jetbackup5d
check_service_exists jetmongod
check_service_exists cagefs
check_service_exists dovecot
check_service_exists exim
check_service_exists pdns
check_service_exists postgresql
check_service_exists pure-ftpd

echo -e "\n##### Monitoring #####"
if grep -q "^brixlymonitoring:" /etc/trueuserowners; then
    pass "User exists"
else
    fail "User does not exist"
fi

if $(curl -I --insecure "https://localhost/~brixlymonitoring/" 2>&1 | grep -I -q index.php); then
    pass "Monitoring page exists"
else
    fail "Monitoring page does not exist"
fi

if [ $(dig brixlymonitoring.$(hostname -s).mysitepreview.co.uk +short) == $(hostname -i) ]; then
    pass "Monitoring DNS exists"
else
    fail "Monitoring DNS is missing"
fi

echo "SSL Certificate:"
true | openssl s_client -connect brixlymonitoring.$(hostname -s).mysitepreview.co.uk:443 2>/dev/null | openssl x509 -noout -subject -checkend 0


if which cagefsctl >/dev/null 2>&1; then
    echo -e "\n##### CageFS Status #####"
    if cagefsctl --cagefs-status >/dev/null 2>&1; then
        pass "CageFS Enabled"
    fi
else
    fail "CageFS Not Enabled"
fi

echo -e "\n##### Default LVE Limits #####"
if [[ $(lvectl list-user | grep "^ default") == " default     200   2048M      0K      30     100   30720    1024" ]]; then
    pass "Default limits set"
else
    fail "Default limits not detected"
fi

echo -e "\n##### Installed Alt-PHP Versions #####"
ls -d /opt/alt/php* | awk -F '/' -e '$4 ~ /php[0-9][0-9]$/ {print $4}'

echo -e "\n##### Installed Alt-Python Versions #####"
ls -d /opt/alt/python* | awk -F '/' -e '$4 ~ /python[0-9][0-9]$/ {print $4}'

echo -e "\n##### Installed Alt-NodeJS Versions #####"
ls -d /opt/alt/alt-node* | awk -F '/' -e '$4 ~ /alt-nodejs[0-9][0-9]$/ {print $4}'
