#!/bin/bash

###################### Logic ######################

# CloudLinux provides a handy tool to tell us the Control Panel
panel=$(cldetect --detect-cp-nameonly)

ENTRY=$(echo "$1" | tr '[:upper:]' '[:lower:]')

if [[ $1 =~ @ ]]; then
    local_part=$(echo "$ENTRY" | awk -F '@' '{print $1}')
    domain=$(echo "$ENTRY" | awk -F '@' '{print $2}')
    email_address=$ENTRY
    user=$(/scripts/whoowns "$domain")
else
    domain=$ENTRY
    user=$(/scripts/whoowns "$domain")
fi

###################### Functions ######################

check_routing () {
    DOMAIN=$1
    PANEL=$2
    USER=$3

    case $PANEL in
        "cPanel")
            # This is what cPanel says it has a record of:
            mx_reference=$(uapi --user="$USER" Email list_mxs domain="$DOMAIN" --output=jsonpretty | jq '.result | .data | .[] | .detected')
            # For cPanel these two files decide where routing is set
            case $(grep -l "^$DOMAIN$" /etc/{localdomains,remotedomains}) in
                "/etc/localdomains /etc/remotedomains")
                    # I think Local takes precedence if both are set, unsure
                    echo "In both local and remote routing."
                    echo "cPanel has this set as $mx_reference"
                    ;;
                "/etc/localdomains")
                    echo "Locally routed domain"
                    echo "cPanel has this set as $mx_reference"
                    ;;
                "/etc/remotedomains")
                    echo "Remote routed domain"
                    echo "cPanel has this set as $mx_reference"
                    ;;
                *)
                    # This should never occur I believe unless its manually removed
                    echo "No domain routing set"
                    echo "cPanel has this set as $mx_reference"
                    ;;
            esac
            echo "If cPanel has this listed as 'auto' or is separate to what is otherwise reported, please ensure after making any changes you update routing."
            # A cPanel account can have a separately set MX record for forwarding purposes (e.g. live transfer proxy    )
            if grep -q "^$DOMAIN:" /etc/manualmx; then
                echo -e "\nDomain has Manual MX record set of:\t%s\n" "$(grep "^$DOMAIN:" /etc/manualmx | awk '{print $2}')"
            fi
            ;;
        "DirectAdmin")
            # For DA either the domain is in this file and is locally routable or its not
            if grep -q "^$DOMAIN$" /etc/virtual/domains; then
                echo "Locally routed domain";
            else
                echo "Remote routed domain";
            fi
            ;;
        "Plesk")
            # For Plesk if they have email disabled it's remote routed, their native tool can tell us if it is
            if [[ $(plesk bin subscription -i "$DOMAIN"  | grep "Mail service:" | awk '{print $3}') == "On" ]]; then
                echo "Locally routed domain";
            else
                echo "Remote routed domain";
            fi
            ;;
        *)
            # If it doesn't match there's no panel
            echo "No control panel found"
    esac
}

dns_check () {
    DOMAIN=$1

    # Don't show if there are no A records
    if [[ $(dig "$DOMAIN" A +short) ]]; then
        # Get A record(s)
        echo -e "A Record(s):\n"
        for record in $(dig "$DOMAIN" A +short); do
            echo -e "$DOMAIN:\t$record\n";
        done
        echo -e "\n"
    fi

    # Don't show if there's no CNAME
    if [[ $(dig "$DOMAIN" CNAME +short) ]]; then
        # Get CNAME record
        echo -e "CNAME Record:\n$DOMAIN:\t$(dig "$DOMAIN" CNAME +short)\n\n"
    fi

    # Get MX record(s)
    echo -e "MX Record(s):\n"
    for record in $(dig "$DOMAIN" MX +short | awk '{print $2}'); do
        # Second dig to get the Priority for the record
        echo -e "$DOMAIN:\t$(dig "$DOMAIN" MX +short | grep " $record" | awk '{print $1}')  $record\n";
    done
    echo -e "\n"
}

get_aliases () {
    DOMAIN=$1
    PANEL=$2

    case "$PANEL" in
        "cPanel")
            # cPanel is just a file
            cat /etc/valiases/"$DOMAIN"
            ;;
        "DirectAdmin")
            # DirectAdmin is just a file too
            cat /etc/virtual/"$DOMAIN"/aliases
            ;;
        "Plesk")
            # Plesk runs Postfix instead and keeps aliases in the DB.. honestly I'm not sure this is even that useful
            plesk db "SELECT a.alias AS alias FROM psa.mail_aliases AS a INNER JOIN psa.mail AS b ON b.id = a.mn_id INNER JOIN psa.domains AS c ON c.id = b.dom_id WHERE c.name LIKE '$DOMAIN';"
            ;;
    esac
}

check_spamexperts () {
    DOMAIN=$1

    echo -e "SpamExperts Destination:\n";
    # Get the destination
    /scripts/spamexpertscli --destination --get "$DOMAIN";
    echo -e "\nSpamExperts Destination Records:\n"
    # Here we split the unneeded bits off the reply - kinda ugly way of doing it
    for record in $(/scripts/spamexpertscli --destination --get "$DOMAIN" | tr -d "\'[],"); do
        # Resolve each record we get
        echo -e "$record:\t$(dig "$record" A +short)\n"
    done
    echo -e "\n"
}

###################### Script ######################

# If we're on a panel that uses exim we can get the details of the email address
if [[ $panel == "cPanel" ]] || [[ $panel == "DirectAdmin" ]] && [[ $email_address ]]; then
    echo -e "Email Details:\n\n$(exim -bt "$email_address")\n\nDomain Details:\n\n"
fi

# Just get the DNS records
dns_check "$domain"

# If the spamexpertscli script exists and one of the spamexperts records is found as an MX record then we get SpamExperts data
if [[ -f /scripts/spamexpertscli && $(dig "$domain" MX +short) =~ mx[12].spamfiltering.io ]]; then
    check_spamexperts "$domain"
fi

# Get Valiases info
echo -e "Valiases:\n"
get_aliases "$domain" "$panel"
echo -e "\n"

# Get routing info
echo -e "Email Routing:\n"
check_routing "$domain" "$panel" "$user"