#!/bin/bash

# Install the plugin
PACKAGE_ID=$(jetbackup5api -F listPackages -D 'find[name]=Imunify360' | grep -oP '_id: \K[^ ]+')
jetbackup5api -F installPlugin -D "package_id=$PACKAGE_ID"

# Wait for a few seconds to ensure the plugin is installed
sleep 5

# Get the plugin ID
PLUGIN_ID=$(jetbackup5api -F listPlugins -D 'filter=imunify' | grep -oP '_id: \K[^ ]+')

# Check if PLUGIN_ID is not empty
if [[ -z "$PLUGIN_ID" ]]; then
    echo "Error: Could not retrieve the plugin ID."
    exit 1
fi

# Enable the plugin using the retrieved ID
jetbackup5api -F managePlugin -D "_id=$PLUGIN_ID&disabled=0"

# Set the security plugin settings
jetbackup5api -F manageSecurityPlugin -D "plugin=$PLUGIN_ID&lock=1&restore=1"

echo "Script completed successfully!"
