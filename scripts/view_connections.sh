#!/bin/bash

if ping google.com -c 1 > /dev/null; then
    echo -e "\nSelf-Updating Script...\n"
    wget -O /scripts/view_connections.sh https://gitlab.com/brixly/file-dump/-/raw/master/scripts/view_connections.sh >/dev/null 2>&1
    echo -e "\n...Update Complete\n"
else
    echo -e "\nNo Network Connectivity, skipping update...\n"
fi

if [[ "$1" == "ssh" ]]; then
    printf "SSH Root via Key:\n"
    grep -E 'sshd.*Accepted.*(publickey|password).*root' /var/log/secure  |awk '{print $1 " " $2 " - " $11}' | sort -k 1,1M -k 2,2n | uniq | grep -v -E '(23.88.43.131|159.69.87.28|109.70.148|23.88.6.16|23.111.182.242)'
    printf "SSH Root via WHM:\n"
    grep -a "session=root" usr/local/cpanel/logs/access_log | grep -v -E '(23.88.43.131|159.69.87.28|109.70.148|23.88.6.16|23.111.182.242|185.53.57.103)' | awk '{print substr($4, 2) " - " $1}' | sort -k 1,1M -k 2,2n | uniq| tail
    exit
fi

printf "\nLSAPI Key Metrics:\n"
grep "lsapi_criu " /etc/apache2/conf.d/lsapi.conf
grep "lsapi_with_connection_pool " /etc/apache2/conf.d/lsapi.conf
grep "lsapi_backend_initial_start " /etc/apache2/conf.d/lsapi.conf
grep "lsapi_reset_criu_on_apache_restart " /etc/apache2/conf.d/lsapi.conf
grep "lsapi_backend_children " /etc/apache2/conf.d/lsapi.conf
grep "lsapi_terminate_backends_on_exit " /etc/apache2/conf.d/lsapi.conf
grep "lsapi_avoid_zombies " /etc/apache2/conf.d/lsapi.conf

printf "\nMySQL Key Metrics:\n"
grep "sql_mode " /etc/my.cnf
grep "key-buffer-size " /etc/my.cnf
grep "query-cache-size " /etc/my.cnf
grep "max-connections " /etc/my.cnf
grep "innodb-buffer-pool-size " /etc/my.cnf

printf "\nMySQL Governor is set to: "
grep lve /etc/container/mysql-governor.xml | awk -F"\"" '{print $2}'

printf "\nCSF Deny List Length:\n"
wc -l /etc/csf/csf.deny

printf "\nCurrent Load / Uptime:\n"
w

printf "\nConnections by Server / Destination IP:\n"
netstat -alpn | grep :80 | awk '{print $4}' |awk -F: '{print $(NF-1)}' |sort | uniq -c | sort -n

printf "\nConnections Breakdown:"
netstat -tn | awk '{print $6}'|sort|uniq -c

printf "\nConnections by External IP:\n"
netstat -ntu | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n | tail

printf "\nApache Domlogs by Size:\n"
ls -lathS /etc/apache2/logs/domlogs/ | head -5

printf "\nLVE Stats / 24h:\n"
lveinfo -d --period=1d --by-fault=any --show-columns="ID,From,To,CPUf,VMemF,EPf,PMemF,NprocF,IOf,IOPSf"

printf "\nLVE Stats for past 60 minutes - EP (See CPUf Column):\n"
lveinfo --by-fault=cpu --period=1h -d --limit=5 --show-columns=id,cpuf

printf "\nLVE Stats for past 60 minutes - EP (See EPf Column):\n"
lveinfo --by-fault=ep --period=1h -d --limit=5 --show-columns=id,epf

printf "\nLVE Stats for past 60 minutes - Memory Usage (See PMemF Column):\n"
lveinfo --by-fault=pmem --period=1h -d --limit=5 --show-columns=id,pmemf

printf "\nLVE Stats for past 60 minutes - IO Usage (See PMemF Column):\n"
lveinfo --by-fault=io --period=1h -d --limit=5 --show-columns=id,iof

printf "\n10 Highest rate-limited domains: \n"
grep limiting /var/log/nginx/error_log | awk -F"server: " '{print $2}' | awk -F"," '{print $1}' | sort | uniq -c | sort -n | tail -10

printf "\n10 Highest rate-limited requests: \n"
grep limiting  /var/log/nginx/error_log | awk -F"request: " '{print $2}' | awk -F"," '{print $1}' | sort | uniq -c | sort -n | tail -10

printf "\n20 Highest Exim Senders: \n"
grep cwd /var/log/exim_mainlog | grep -v /var/spool | awk -F"cwd=" '{print $2}' | awk '{print $1}' | sort | uniq -c | sort -n | tail -20

printf "\n20 Highest Cron Log Entries: \n"
cat /var/log/cron | awk -F"(" '{print $2}' | awk -F")" '{print $1}' | sort | uniq -c | sort -n | tail -20

printf "\n"
read -p "Do you want to check the Apache domlogs for most frequent IP's (USE WITH CAUTION)? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cat /etc/apache2/logs/domlogs/*.* | awk '{print $1}' | sort | uniq -c | sort -n | tail -20 > /tmp/freqips.txt
    printf "\n"
    read -p "Do you want me to suspend the top 10 IP's? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        for ip in $(awk '{print $2}' /tmp/freqips.txt); do imunify360-agent blacklist ip add "$ip" --scope group --comment "Large Connections in Domlogs - view_connections"; done;
    fi
fi

printf "\n"
read -p "Do you want to check for suspicious processes? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    /opt/nDeploy/nDeploy_whm/abnormal_process_detector.cgi
fi

printf "\n"
read -p "Do you want to check Apache status via lynx? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    lynx http://localhost/whm-server-status
fi

printf "\n"
read -p "Do you want to check Nginx status via lynx? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    lynx http://localhost/nginx_status
fi

printf "\n"
read -p "Do you want to check running MySQL processes? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mysqladmin pr
fi

printf "\n"
read -p "Do you want to tail the logs? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    multitail /etc/apache2/logs/error_log /var/log/nginx/error_log /var/log/lfd.log /var/log/messages
fi

printf "\n"
read -p "Do you want to block most common IP's in TCP / UDP (Use with Caution)? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    for ip in $(grep TCP_IN /var/log/messages | awk -F"SRC=" '{print $2}' | awk '{print $1}' | sort | uniq -c | sort -n | tail -20 | awk '{print $2}'); do csf -d "$ip" "Too many TCP_IN Entries in messages logs"; done;
    for ip in $(grep TCP_OUT /var/log/messages | awk -F"DST=" '{print $2}' | awk '{print $1}' | sort | uniq -c | sort -n | tail -20 | awk '{print $2}'); do csf -d "$ip" "Too many TCP_OUT Entries in messages logs"; done;
fi

printf "\n"
read -p "Do you want to perform a full security analysis (CSI)? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    wget -O /scripts/csi.py https://github.com/CpanelInc/tech-CSI/raw/master/csi.pl
    chmod 655 /scripts/csi.py
    /scripts/csi.py
fi

printf "\n"
read -p "Do you want to run a system status probe? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    wget -O /scripts/ssp https://github.com/CpanelInc/SSP/raw/master/ssp
    chmod 655 /scripts/ssp
    /scripts/ssp
fi
