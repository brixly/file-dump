#!/bin/bash

if [ -z $1 ]; then
    echo "Takes one argument for a domain name"
    exit 1
fi

DOMAIN=$1
OWNER=$(/scripts/whoowns $DOMAIN)

if [ -z $OWNER ]; then
    echo "Domain owner not found!"
    exit 1
fi
    
mkdir -p /etc/apache2/conf.d/userdata/std/2_4/$OWNER/$DOMAIN
mkdir -p /etc/apache2/conf.d/userdata/ssl/2_4/$OWNER/$DOMAIN

echo "<IfModule LiteSpeed>
   LsRecaptcha 100
</IfModule>
" > /etc/apache2/conf.d/userdata/std/2_4/$OWNER/$DOMAIN/enable_captcha.conf

echo "<IfModule LiteSpeed>
   LsRecaptcha 100
</IfModule>
" > /etc/apache2/conf.d/userdata/ssl/2_4/$OWNER/$DOMAIN/enable_captcha.conf

/usr/local/cpanel/scripts/rebuildhttpdconf

if [[ $(echo $?) == 0 ]]; then
    echo "Apache conf build passed"
else
    echo "Apache conf rebuild failed"
fi
