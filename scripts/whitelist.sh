#/bin/bash

if [ "$#" -eq 0 ] ; then
        echo "Please provide an IP address as the first parameter"
elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        echo "Removing $1 from blacklist"
        imunify360-agent blacklist ip delete $1
        echo "Whitelisting $1 in group"
        imunify360-agent whitelist ip add $1 --scope group
else
        echo "Invalid number of parameters"
fi
