#!/bin/bash

help () {
    printf 'Usage: /scripts/pkgswap $OLDACCOUNT $NEWACCOUNT\n'
}

if [[ $# -ne 2 ]]; then
    help
    exit 0
fi

# The accounts to replace
OLDACCOUNT="$1"
NEWACCOUNT="$2"

IFS=$'\n'
# Rename all the packages
for PACKAGE in `ls /var/cpanel/packages | grep $OLDACCOUNT\_`; do { PACKAGENAME=`echo "$PACKAGE" | cut -d_ -f2`; mv -v "/var/cpanel/packages/$PACKAGE" "/var/cpanel/packages/$NEWACCOUNT_$PACKAGENAME"; }; done

# Rename all the feature lists
for FEATURE in `ls /var/cpanel/features | grep $OLDACCOUNT\_`; do { FEATURENAME=`echo "$FEATURE" | cut -d_ -f2`; mv -v "/var/cpanel/features/$FEATURE" "/var/cpanel/features/$NEWACCOUNT_$FEATURENAME"; }; done
unset IFS

exit 1
