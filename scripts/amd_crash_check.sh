#!/bin/bash

logfile="/var/log/kern.log"
search_string="Completion-Wait loop timed out"
webhook_url="https://mattermost.brixly.uk/hooks/gru6tyfhybgotbobgw8bnrr3dy"

function post_to_mattermost {
    curl -i -X POST -H 'Content-Type: application/json' -d "{
        \"text\": \"Hostname: $(hostname)\nLOG: ${LOGLINE} - DETECTED\nCEPH has had the Proxmox noout flag set in preparation for a crash\nIMPORTANT: You MUST unset the noout flag when the server is back online following a reboot. The command to unset the noout flag is 'ceph osd unset noout'\"
    }" $webhook_url
}

function set_noout {
    ceph osd set noout
}

action_performed=0

tail -F "$logfile" | while read LOGLINE
do
   if [[ "${LOGLINE}" == *"$search_string"* ]] && [ "$action_performed" -eq 0 ]; then
       post_to_mattermost
       set_noout
       action_performed=1
   fi
done
