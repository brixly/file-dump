#!/bin/bash
# curl -sSL https://gitlab.com/brixly/file-dump/-/raw/master/scripts/setup_root_login_monitor.sh | bash
# Create the check_root_login.sh file
touch /usr/local/bin/check_root_login.sh

# Add the required contents to the file
cat > /usr/local/bin/check_root_login.sh << 'EOL'
#!/bin/bash

WEBHOOK_URL="https://mattermost.brixly.uk/hooks/ghxk9kqg7b81pm9y6cu9wez7zr"

last_cpanel_login=$(cat /usr/local/cpanel/logs/access_log | grep -a "session=root\b" | grep -v -E '(23.88.43.131|159.69.87.28|109.70.148|23.88.6.16|23.111.182.242|185.53.57.103|131.226.0.65)' | awk '{print substr($4, 2) " - " $1}' | sort -k 1,1M -k 2,2n | uniq | tail -n 1)
last_ssh_login=$(grep 'sshd.*Accepted.*root' /var/log/secure | grep -v -E '(23.88.43.131|159.69.87.28|109.70.148)' | tail -n 1 | awk '{print $1" "$2" "$3" - "$11}')

last_login="$last_cpanel_login;$last_ssh_login"

if [ ! -f /root/last_root_login.txt ]; then
  echo "$last_login" > /root/last_root_login.txt
  exit 0
fi

stored_login=$(cat /root/last_root_login.txt)

if [ "$last_login" != "$stored_login" ]; then
  IFS=';' read -ra new_logins <<< "$last_login"
  IFS=';' read -ra old_logins <<< "$stored_login"
  if [ "${new_logins[0]}" != "${old_logins[0]}" ]; then
    ip=$(echo "${new_logins[0]}" | awk '{print $NF}')
    hostname_info=$(host "$ip")
    whois_info=$(whois "$ip" | grep -E 'OrgName|Country|netname|descr')
    curl -X POST -H 'Content-Type: application/json' -d "{\"text\": \"New root login detected on $(hostname) (cPanel): ${new_logins[0]}\\nHostname Info: $hostname_info\\nWhois Info:\\n$whois_info\"}" $WEBHOOK_URL
  fi
  if [ "${new_logins[1]}" != "${old_logins[1]}" ]; then
    ip=$(echo "${new_logins[1]}" | awk '{print $NF}')
    hostname_info=$(host "$ip")
    whois_info=$(whois "$ip" | grep -E 'OrgName|Country')
    curl -X POST -H 'Content-Type: application/json' -d "{\"text\": \"New root login detected on $(hostname) (SSH): ${new_logins[1]}\\nHostname Info: $hostname_info\\nWhois Info:\\n$whois_info\"}" $WEBHOOK_URL
  fi
  echo "$last_login" > /root/last_root_login.txt
fi
EOL

# Make the script executable
chmod +x /usr/local/bin/check_root_login.sh

# Add the cron job to execute the script every 1 minute
(crontab -l 2>/dev/null | grep -q '/usr/local/bin/check_root_login.sh') || (echo "$(crontab -l 2>/dev/null)"$'\n'"* * * * * /usr/local/bin/check_root_login.sh") | crontab -

echo "Root login monitor setup complete."
