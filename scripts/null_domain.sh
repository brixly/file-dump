#!/bin/bash

# Check if domain is provided
if [ -z "$1" ]; then
    echo "Error: Please provide a domain as the first parameter."
    exit 1
fi

DOMAIN="$1"
DOMAIN_ROOT=$(/scripts/get-root.py "$DOMAIN")
HTACCESS_PATH="$DOMAIN_ROOT/.htaccess"
HTACCESS_DDOS_PATH="$DOMAIN_ROOT/.htaccess.ddos"

# Create or remove .htaccess based on the second parameter
if [ "$2" == "--remove" ]; then
    if [ -f "$HTACCESS_DDOS_PATH" ]; then
        mv -f "$HTACCESS_DDOS_PATH" "$HTACCESS_PATH"
        echo "Restored .htaccess file from .htaccess.ddos."
    else
        echo "Error: .htaccess.ddos file not found."
    fi
else
    # Move existing .htaccess to .htaccess.ddos
    if [ -f "$HTACCESS_PATH" ]; then
        mv -f "$HTACCESS_PATH" "$HTACCESS_DDOS_PATH"
    fi

    # Create new .htaccess with the specified content
    cat << EOF > "$HTACCESS_PATH"
RewriteEngine On
RewriteRule .* - [E=blockbot:1]
EOF

    # Set file permissions to be editable only by root
    chown root:root "$HTACCESS_PATH"
    chmod 600 "$HTACCESS_PATH"

    echo "Created new .htaccess file with blockbot rule and moved existing .htaccess to .htaccess.ddos."
fi
