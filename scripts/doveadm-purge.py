#!/usr/bin/python3

from threading import Thread
import queue 
import time
import os
import json
import subprocess

class TaskQueue(queue.Queue):

    def __init__(self, num_workers=1):
        queue.Queue.__init__(self)
        self.num_workers = num_workers
        self.start_workers()

    def add_task(self, task, *args, **kwargs):
        args = args or ()
        kwargs = kwargs or {}
        self.put((task, args, kwargs))

    def start_workers(self):
        for i in range(self.num_workers):
            t = Thread(target=self.worker)
            t.daemon = True
            t.start()

    def worker(self):
        while True:
            item, args, kwargs = self.get()
            item(*args, **kwargs)  
            self.task_done()


def tests():

    def purge(domain, owner):
        try:
            users = [name for name in os.listdir(f"/home/{owner}/mail/{domain}") if os.path.isdir(f"/home/{owner}/mail/{domain}/{name}")]
            for user in users:
                print(f"Purging {user}@{domain}")
                subprocess.getoutput(f"doveadm purge -u {user}@{domain}")
        except:
            pass

    domains = json.loads(subprocess.getoutput("whmapi1 --output=json get_domain_info"))

    q = TaskQueue(num_workers=5)

    for domain in domains['data']['domains']:
        q.add_task(purge, domain['domain'], domain['user'])

    q.join()       # block until all tasks are done
    print("All done!")

if __name__ == "__main__":
    tests()