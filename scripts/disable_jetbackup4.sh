# /bin/bash

# bash <(wget -qO- https://gitlab.com/brixly/file-dump/-/raw/master/scripts/disable_jetbackup4.sh)

whmapi1 update_featurelist featurelist=disabled jetbackupsingle=0
whmapi1 update_featurelist featurelist=disabled jetbackupcronbackups=0
whmapi1 update_featurelist featurelist=disabled jetbackupdbbackups=0
whmapi1 update_featurelist featurelist=disabled jetbackupdnsbackups=0
whmapi1 update_featurelist featurelist=disabled jetbackupemailbackups=0
whmapi1 update_featurelist featurelist=disabled jetbackupfilesbackups=0
whmapi1 update_featurelist featurelist=disabled jetbackupfullbackups=0
whmapi1 update_featurelist featurelist=disabled jetbackupgdpr=0
whmapi1 update_featurelist featurelist=disabled jetbackupqueue=0
whmapi1 update_featurelist featurelist=disabled jetbackupsettings=0
whmapi1 update_featurelist featurelist=disabled jetbackupsnapshots=0
whmapi1 update_featurelist featurelist=disabled r1softcdp=0
whmapi1 update_featurelist featurelist=disabled r1soft=0
