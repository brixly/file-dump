#!/bin/bash

user_list=$(sasldblistusers2 /etc/sasllsmcd | cut -d@ -f1)
#get current user list

for name in $(ls /home/);
do
    if [[ -d /home/$name/public_html ]] ; then
    #check public_html existance to make sure it's vhost user instead of cPanel created dir
        if ! echo $user_list | grep -i -q $name ; then    
            #check if user already in the list to avoid override existing users
            passwd=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')
            echo $passwd | saslpasswd2 -p -f /etc/sasllsmcd $name
            # use -p to set a random password without prompt
            echo "$name added into LSMCD"
        else
            echo "$name already in the list..."
        fi
    fi
done
