echo -e "Disk Usage (Before): \n"
df -h | grep T

echo -e "Cleaning DirectAdmin User Backups: \n"
rm -rf /home/*/user_backups/*

echo -e "Cleaning cPanel Backups: \n"
rm -f /home/*/backup-*
rm -f /home/*/cpmove-*
rm -f /home/cpmove-*
rm -f /home/backup-*
find /backup -name "cpmove-*" -delete
rm -Rf /var/www/html/cpmove*

echo -e "Cleaning Cpanel_* Files / Temps: \n"
rm -f /home/*/tmp/Cpanel_*
#rm -Rf /home/*/.cagefs/tmp/* - Removed, as it affects mysql.sock

echo -e "Clean backups from /root: \n"
find /root -size +1G -name "cpmove*" -delete


echo -e "Cleaning Old Logs: \n"
find /var/log -name "*.gz" -print -delete
find /var/log/server-status/ -mtime +1 -type d -exec rm -Rf {} \;
rm -f /var/log/exim_mainlog-*
rm -f /var/log/exim_paniclog-*
rm -f /var/log/exim_rejectlog-*
rm -f /var/log/lve-stats.log-*
rm -f /usr/local/jetapps/usr/jetbackup5/downloads/*
rm -f /var/log/nginx/*.gz
rm -f /usr/local/apache/logs/*.gz
rm -f /usr/local/apache/logs/archive/*.gz
rm -f /usr/local/cpanel/logs/*.gz
rm -f /usr/local/cpanel/logs/archive/*.gz

echo -e "Cleaning /usr/local/src: \n"
rm -f /usr/local/src/*

echo -e "Cleaning cPanel Trash Backups: \n"
rm -rf /home/*/.trash/*

echo -e "Cleaning Various Backups and Caches: \n"
rm -rf /home/*/backupbuddy_backups/*
rm -rf /home/*/com_akeeba/backup/*
rm -rf /home/*/softaculous_backups

find /home -type d \
  \( \
    -path "*/wp-content/backups" \
    -o -path "*/wp-content/wpvividbackups" \
    -o -path "*/wp-content/cache/flying-press" \
    -o -path "*/wp-content/updraft" \
  \) \
  -exec rm -rf {} +

echo -e "Cleaning Additional files and Error Logs: \n"

find /home \
  -not -path "*/com_akeebabackup/*" -not -path "*/com_akeeba/*" \
  -type f \( \
    -iname "duplicati*.aes" -o \
    -iname "stderr.log" -o \
    -iname "xmrig" -o \
    -iname ".well-known.zip" -o \
    -iname "prod-error.log" -o \
    -iname "dev-error.log" -o \
    -iname "dev.log" -o \
    -iname "laravel.log" -o \
    -iname "server.log" -o \
    -iname "error_log-*" -o \
    -iname "*.wpress" -o \
    -iname "*.rar" -o \
    -iname "*.iso" -o \
    -iname "*.xen" -o \
    -iname "*.7z" -o \
    -iname "*.tar*" -o \
    -iname "*.mkv" -o \
    -iname "*.avi" -o \
    -iname "*.pbo" -o \
    -iname "*.VOB" -o \
    -iname "error_log" -o \
    -iname "*_backup" -o \
    -iname "backup*.zip" -o \
    -iname "php_error_log" -o \
    -iname "*.zi" -o \
    -iname "public_html.zip" -o \
    -iname "wp-admin.zip" -o \
    -iname "mail.zip" -o \
    -iname "email.zip" -o \
    -iname "*.h264" -o \
    -iname "*.zst" -o \
    -iname "*.jpa" -o \
    -iname "1.mcm.x86_64" -o \
    -name "magick-*" \
  \) -delete

find /home -type f -name "log-*.php" -size +50M -delete
find /home -type f -name "*.zip" -size +5120M -delete
#find /home -type f -name "*.log" -size +1024M -delete

find /home -type f ! -name "*.*" -size +1G -exec file -i {} \; | grep application/zip | cut -d: -f1 | xargs rm

echo -e "Cleaning Junk Mail: \n"
MAILDIRS=$(find /home/*/mail/*/* -maxdepth 0 -type d)
INBOXFOLDERS=(.Trash .Junk .Spam .Low\ Priority .cPanel\ Reports)

for basedir in $MAILDIRS; do
    for folder in "${INBOXFOLDERS[@]}"; do
        for dir in cur new; do
            folder_path="$basedir/$folder/$dir"
            if [ -e "$folder_path" ]; then
                echo "Processing $folder_path…"
                find "$folder_path" -type f -mtime +30 -delete
            fi
        done
    done
done

/scripts/generate_maildirsize --allaccounts --confirm

echo -e "Disk Usage (After): \n"
df -h | grep T
