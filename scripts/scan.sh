if [ "$#" -eq 0 ] ; then

        printf "\n"
        read -p "Do you want to run a scan of the entire server? " -n 1 -r
        echo    # (optional) move to a new line
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
                imunify360-agent malware on-demand start --path /home/
        fi

elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        echo $1|grep '@'
        if [ $? -eq 0 ] ; then
                whmapi1 create_user_session user=$arg service=webmaild
        else
                printf "Scan initiated for $arg...\n \n"
                imunify360-agent malware on-demand start --path /home/$arg/public_html/
                printf "\nPreviously caught malware: \n \n"
                imunify360-agent malware malicious list --user $arg --limit 500
                #fi
        fi
else
        echo "Invalid number of parameters"
fi
