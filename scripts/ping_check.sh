#!/bin/bash

ping -c 2 $1 > /dev/null && echo $1 " - up" || echo $1 " - DOWN"
