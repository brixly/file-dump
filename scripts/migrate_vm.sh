#!/bin/bash

# Prompt for Source Hostname
read -p "Please enter the source hostname: " hostname

# Retrieve the list of logical volumes from the source server
echo "Retrieving list of logical volumes from $hostname..."
mapfile -t lv_array < <(ssh root@$hostname "lvdisplay -C -o lv_path --noheadings" | grep -v cloudinit | awk '{$1=$1};1')

# Display the logical volumes and prompt for selection
echo "Please select the logical volume to transfer:"
for i in "${!lv_array[@]}"; do
          echo "$((i+1)). ${lv_array[i]}"
  done
  read -p "Enter the number of the logical volume: " lv_selection

  # Validate the selection
  if [[ "$lv_selection" -lt 1 ]] || [[ "$lv_selection" -gt "${#lv_array[@]}" ]]; then
            echo "Invalid selection. Exiting."
              exit 1
  fi

  # Get the selected logical volume
  src_lv=${lv_array[$lv_selection-1]}

  # Retrieve the size of the logical volume
  lv_size=$(ssh root@$hostname "blockdev --getsize64 $src_lv")

  # Retrieve the list of logical volumes from the destination server (this server)
  echo "Retrieving list of logical volumes from the local server..."
  mapfile -t dst_lv_array < <(lvdisplay -C -o lv_path --noheadings | awk '{$1=$1};1')

  # Display the logical volumes and prompt for selection
  echo "Please select the destination logical volume to transfer to:"
  for i in "${!dst_lv_array[@]}"; do
            echo "$((i+1)). ${dst_lv_array[i]}"
    done
    read -p "Enter the number of the logical volume: " dst_lv_selection

    # Validate the selection
    if [[ "$dst_lv_selection" -lt 1 ]] || [[ "$dst_lv_selection" -gt "${#dst_lv_array[@]}" ]]; then
              echo "Invalid selection. Exiting."
                exit 1
    fi

    # Get the selected logical volume
    dst_lv=${dst_lv_array[$dst_lv_selection-1]}

    # Run the data transfer with progress
    ssh root@$hostname "dd if=$src_lv bs=4096" | pv -s $lv_size | dd of=$dst_lv bs=4096

    echo "Data transfer completed."
