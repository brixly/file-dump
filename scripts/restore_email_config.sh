#!/bin/bash

if [ -z $1 ]; then
    echo "Takes one argument for a domain name"
    exit 1
fi

DOMAIN=$1
OWNER=$(/scripts/whoowns $DOMAIN)

if [ -z $OWNER ]; then
    echo "Domain owner not found!"
    exit 1
fi

if [ -f "/home/$OWNER/etc/$DOMAIN/passwd" ]; then
    read -p "Passwd file detected, proceed anyway? [y/n]  " passwd_confirm
fi

if [ -f "/home/$OWNER/etc/$DOMAIN/passwd" ]; then
    read -p "Shadow file detected, proceed anyway? [y/n]  " shadow_confirm
fi

if [[ $passwd_confirm && $passwd_confirm != "y" ]] || [[ $shadow_confirm && $shadow_confirm != "y" ]]; then
    exit 0
fi

touch "/home/$OWNER/etc/$DOMAIN/passwd"
touch "/home/$OWNER/etc/$DOMAIN/shadow"

echo "Added to passwd"
for i in $(ls /home/$OWNER/mail/$DOMAIN); do
    echo "$i:x:$(id -u $OWNER):$(id -g $OWNER)::/home/$OWNER/mail/$DOMAIN/$i:/home/$OWNER" | tee -a /home/$OWNER/etc/$DOMAIN/passwd
done

echo "Added to shadow"
for i in $(ls /home/$OWNER/mail/$DOMAIN); do
    echo "$i:\$6\$PsiWKtn4PVClBmEy\$C9Cq2iHxEzIjrowCBglfKIzFO4Oakp6KAAgEvmkgjozfnaTZ7Pf3opzLh6XrTVyOR7Z40y6J55uF8D14G.rTH1:19493::::::" | tee -a /home/$OWNER/etc/$DOMAIN/shadow
done

chown $OWNER.mail /home/$OWNER/etc/$DOMAIN/passwd
chown $OWNER.$OWNER /home/$OWNER/etc/$DOMAIN/shadow

rm -f /home/$OWNER/.cpanel/email_accounts.json
rm -f /home/$OWNER/.cpanel/email_accounts_count

doveadm auth cache flush
