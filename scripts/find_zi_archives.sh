#/bin/bash
for file in $(find -type f ! -name "*.*" -name "z*" ! -name "zend*")
do 
    file $file | grep Zip | awk -F":" '{print $1}'
done
