#!/bin/bash
ID=$1

# Check that an argument was entered
if [ -z "$ID" ]; then
	echo "Missing VM ID! Please add as first arg"
	exit 1
fi

if [ ! -f "/etc/solus_api" ]; then
	echo "API Key missing from /etc/solus_api!"
	exit 1
fi

key=$(cat /etc/solus_api)

# Fetch UUID
serverInfo=$(curl -q -X GET \
	-H "authorization: Bearer $key" \
	"https://panel.equilia.cloud/api/v1/servers/$ID" 2>/dev/null
)

# Check if the curl output provides a working UUID
if [[ $(echo "$serverInfo" | jq -r .[]) == "Route not found" ]]; then
	echo "VM UUID for given ID not found!"
	exit 1
elif [[ $(echo "$serverInfo" | jq -r .[]) =~ "No query results for model" ]]; then
	echo "VM UUID for given ID not found!"
	exit 1
else
	UUID=$(echo "$serverInfo" | jq -r .data.uuid)
	echo "VM UUID found $UUID"
fi

# File containing firewall rules
FILE=/usr/local/solus/ovs/$UUID/rules

# Test if rules were already edited
if [ -f "$FILE.old" ]; then
	echo "Old file $FILE.old already exists, exiting..."
	exit 1
fi

# Move current ruleset to a backup
cp -v "$FILE" "$FILE.old"

# Change rules to enable firewall
echo "Starting sed..."
sed -i '/tcp_dst=25\s/d' "$FILE"
sed -i '/tcp_dst=465\s/d' "$FILE"
sed -i '/tcp_dst=587\s/d' "$FILE"

sed -i '/tcp6_dst=25\s/d' "$FILE"
sed -i '/tcp6_dst=465\s/d' "$FILE" 
sed -i '/tcp6_dst=587\s/d' "$FILE"
echo "...finished editing! Please reboot server to apply changes."
exit 0

