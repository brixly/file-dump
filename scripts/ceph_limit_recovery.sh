#!/bin/bash

# Check if an argument was passed
if [ $# -eq 0 ]; then
    echo "Usage: $0 <value>"
    exit 1
fi

# Check if argument is a number between 50 and 2000
if ! [[ $1 =~ ^[0-9]+$ ]] || [ $1 -lt 50 ] || [ $1 -gt 2000 ]; then
    echo "Error: Value must be a number between 50 and 2000"
    exit 1
fi

# Check if value is higher than 1000 and display warning
if [ $1 -gt 1000 ]; then
    echo "Please note that values between 200-800 have been tested as 'safe'. Using a higher value will speed the CEPH sync process up, but may cause unexpected issues with IO on your VM's"
fi

# Run ceph command with parameter value
ceph tell osd.* injectargs "--osd_mclock_scheduler_background_recovery_res=$1"
ceph tell osd.* injectargs "--osd_mclock_scheduler_background_recovery_lim=$1"
