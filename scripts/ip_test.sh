#!/bin/bash

if [ -z $1 ]; then
    echo "Takes one IPv4 as an argument"
    exit 1
fi

IP=$1

if ! $(echo $IP | grep -Pq "\d+\.\d+\.\d+\.\d+"); then
	echo "Input doesn't match IPv4, exiting..."
	exit 1
fi

echo -e "Checking Imunify:\n"
imunify360-agent blacklist ip list --by-ip $IP

if $(which csf > /dev/null 2>&1); then
	echo -e "\nChecking CSF:\n"
	csf -g $IP
fi

echo -e "\nChecking LiteSpeed:\n"
/scripts/lsws_blocked.sh | grep $IP