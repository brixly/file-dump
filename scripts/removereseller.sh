#!/bin/bash
if [ "$#" -eq 0 ] ; then
        echo "Please specify the reseller username to proceed"
elif [ "$#" -eq 1 ] ; then
        arg=$(echo $1)
        sed -i 's/auto_del_domain=*/auto_del_domain=0/g' /etc/prospamfilter/settings.conf
        echo $1|grep '@'
        for user in $(cat /etc/trueuserowners | grep -w $arg | awk -F":" '{print $1}'); do /scripts/removeacct --force --keepdns $user; done;
        sed -i 's/auto_del_domain=*/auto_del_domain=1/g' /etc/prospamfilter/settings.conf
fi
