#!/bin/bash

# /scripts/whoowns script for DA

DOMAIN=$(echo $1 | sed 's/www.//')

# Check one domain has been provided or help output
if [ -z $1 ] || [ $1 == "--help" ] || [ $1 == "-h" ]; then
    echo "Takes one argument as domain name."
    exit 1
# Check to see if the domain is primary
elif grep -q "^$DOMAIN:" /etc/virtual/domainowners; then
    echo "$(grep "^$DOMAIN:" /etc/virtual/domainowners | awk '{print $2}')"
    exit 0
else
    # Strip subdomain
    PRIMARY=$(echo $DOMAIN | cut -d'.' -f2-)
    # See if remaining is a primary domain
    if grep -q "^$PRIMARY:" /etc/virtual/domainowners; then
        POTENTIALOWNER=$(grep "^$PRIMARY:" /etc/virtual/domainowners | awk '{print $2}')
        # Test to see if a subdomain for the primary matches
        for SUBDOMAIN in $(cat /usr/local/directadmin/data/users/$POTENTIALOWNER/domains/$PRIMARY.subdomains); do
            if [ "$SUBDOMAIN.$PRIMARY" == $1 ]; then
                # To make other scripts simpler this can output if it's a subdomain
                if [[ $2 == "--subdomain" ]] || [[ $2 == "-s" ]]; then
                    echo "$POTENTIALOWNER subdomain"                    
                else
                    echo $POTENTIALOWNER
                fi
                exit 0
            fi
        done
    fi
    exit 1
fi
