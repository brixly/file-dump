#!/bin/bash
# With Love from Liam and Sam <3
if ping google.com -c 1 > /dev/null; then
    wget -O /scripts/find_jank.sh https://gitlab.com/brixly/file-dump/-/raw/master/scripts/find_jank.sh >/dev/null 2>&1
fi
chmod +x /scripts/find_jank.sh
ps aux | \
    awk '$1 !~ "root|lsadm|nobody|rpc|dovecot|cpanel|dovenull|mailman|mailnull|imunify|_imunif|postgres|nscd|polkitd|chrony|daemon|dbus|redis|ossec|named|mysql|mongod" {print $0}' | \
    grep -vE "cpsrvd|pure-ftpd|dovecot|lsphp|php-fpm|proxyexec|webmaild|sw-engine|cpanellogd|spamd|lsnode|cpaneld|webalizer|redis-server|php-cgi|gpg|ssh|virtualenv|/opt/alt|/opt/cpanel|vscode-server|ping|npm|bash|cagefs_enter|wp-toolkit|softaculous|logrunner|wget|cpdavd|elasticsearch|/usr/bin/systemd/systemd|bin/analog|/usr/lib/systemd/systemd|artisan|sd-pam|proftpd:"