#!/bin/bash

if test -f "./wp-config.php"; then
    OWNER=$(stat -c '%U' "./wp-config.php")
    if $(su -s /bin/bash - $OWNER  -c "wp config get WP_DEBUG --path=$(pwd)") == true; then
        su -s /bin/bash - $OWNER  -c "wp config set WP_DEBUG false --path=$(pwd)"
    else
        su -s /bin/bash - $OWNER  -c "wp config set WP_DEBUG true --path=$(pwd)"
    fi
else
    echo "wp-config.php not found!"
