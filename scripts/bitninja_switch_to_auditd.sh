# /bin/bash
bitninjacli --module=MalwareDetection --use-auditd

cat <<EOT >> /etc/bitninja/MalwareDetection/config.ini

[FileSystemMonitor]
monitor_type = 'auditd'
monitor_order[] = 'auditd'
monitor_order[] = 'inotify'
monitor_order[] = 'nullMonitor'

EOT

sed -i "/freq/c\freq = 500" /etc/audit/auditd.conf
sed -i "/write_logs/c\write_logs = no" /etc/audit/auditd.conf

service auditd restart
service bitninja restart
