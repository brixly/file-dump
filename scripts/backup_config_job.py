#! /usr/bin/python3

import json
import subprocess

destinations=json.loads(subprocess.getoutput("jetbackup5api -F listDestinations -O json"))

for i in destinations['data']['destinations']:
    if "your-storagebox.de" in i['options']['host']:
        storageid=i['_id']

schedules=json.loads(subprocess.getoutput("jetbackup5api -F listSchedules -O json"))

for i in schedules['data']['schedules']:
    if i['type'] == 2:
        scheduleid=i['_id']

backupjob=json.loads(subprocess.getoutput(f"jetbackup5api -F manageBackupJob -D 'action=create&type=2&structure=1&name=Config Files&destination[0]={storageid}&contains=3&include_list[]=/etc/passwd&include_list[]=/etc/shadow&include_list[]=/etc/group&include_list[]=/etc/gshadow&include_list[]=/etc/trueuserowners&include_list[]=/etc/trueuserdomains&include_list[]=/etc/userdomains&schedules[0][_id]={scheduleid}&schedules[0][retain]=30' -O json"))
