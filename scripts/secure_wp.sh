#!/bin/bash

repair_wp() {

        username=$1
        FILE=/home/$username/public_html/wp-config.php
        if test -f "$FILE"; then
            echo "$FILE - found Installation."
            PASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
            DB_USER=$(cat $FILE | grep DB_USER | awk -F"'" '{print $4}')
            DB_PASS=$(cat $FILE | grep DB_PASSWORD | awk -F"'" '{print $4}')
            sed -i "/DB_PASSWORD/c\define('DB_PASSWORD', '$PASS');" $FILE
            uapi --user=$username Mysql set_password user=$DB_USER password=$PASS
            echo "$FILE updated with new details - new pass: $PASS"
            su -s /bin/bash - $username -c "wp plugin activate --all --path=/home/$username/public_html"
        fi      
}

if [ "$#" -eq 0 ] ; then

        echo "Please specify the username to proceed"

fi

if [ "$#" -eq 1 ] ; then

        username=$(echo $1)
        repair_wp $username

fi

if [ "$#" -eq 2 ] ; then

        reseller=$(echo $1)
        flag=$(echo $2)
        if [ $flag = '--reseller' ]; then
            echo "$username is a reseller - looping all accounts..."
            for user in $(cat /etc/trueuserowner | grep -w $reseller | awk -F":" '{print $1}'); do repair_wp $user; done;
        fi

fi
