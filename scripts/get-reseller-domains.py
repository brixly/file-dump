#!/usr/bin/python3

import json
import subprocess
import sys
import requests

try:
	panel=subprocess.getoutput("cldetect --detect-cp-nameonly")
except:
	panel="No CL Found"

user=sys.argv[1]

if panel == "cPanel":

	resellerdata=json.loads(subprocess.getoutput(f"whmapi1 resellerstats user={user} --output=json"))

	if resellerdata['metadata']['result'] == 0:
		sys.exit(f"Failed! Error is {resellerdata['metadata']['reason']}")

	for account in resellerdata['data']['reseller']['acct']:
		print(subprocess.getoutput(f"grep \': {account['user']}$\' /etc/userdomains | cut -d: -f1"))

elif panel == "DirectAdmin":
	adminAuth=subprocess.getoutput("/usr/local/directadmin/directadmin --root-auth-url-for=admin")

	resellers=json.loads(requests.get(f"{adminAuth}/CMD_API_SHOW_RESELLERS?json=yes").text)

	if user not in resellers:
		sys.exit(f"Failed! {user} not found in list of resellers.")
	
	userList=json.loads(requests.get(f'{adminAuth}/CMD_API_SHOW_USERS?reseller={user}&json=yes').text)
	
	for ownedUser in userList:
		userAuth=subprocess.getoutput(f'/usr/local/directadmin/directadmin --root-auth-url-for={ownedUser}')
		print(*json.loads(requests.get(f'{userAuth}/CMD_API_SHOW_DOMAINS?json=yes').text), sep='\n')

else:
	print("Panel either not detected or not supported!")