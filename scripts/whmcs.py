#!/usr/bin/python3

import json
import subprocess
import sys
import os
import shutil

if len(sys.argv) != 2 and len(sys.argv) != 3:
    sys.exit(f"Usage: {sys.argv[0]} username [channel]")

if len(sys.argv) == 3:
    channel = sys.argv[2]

username = sys.argv[1]

if not os.path.isdir(f"/home/{username}"):
    sys.exit(f"Either user {username} does not exist or their home directory is missing!")

if "channel" in locals():
    latest = json.loads(subprocess.getoutput(f"curl -s https://api1.whmcs.com/download/latest?type={channel}"))
else:
    latest = json.loads(subprocess.getoutput("curl -s https://api1.whmcs.com/download/latest?type=stable"))

url = latest['url']

filename = url.split("/")[-1]

subprocess.getoutput(f"wget {url} --quiet -O /home/{username}/{filename}")

shutil.chown(f"/home/{username}/{filename}", username, username)

print(f"Completed! File path is\n/home/{username}/{filename}")
