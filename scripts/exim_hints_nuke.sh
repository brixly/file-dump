#!/usr/bin/bash

/usr/sbin/exim_tidydb -t 1d /var/spool/exim retry

service exim stop
 
rm -fv /var/spool/exim/db/*

service exim start