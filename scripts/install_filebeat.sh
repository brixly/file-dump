#!/bin/bash

# Download and install Filebeat
curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.16.3-x86_64.rpm
sudo rpm -vi filebeat-7.16.3-x86_64.rpm

# Download the new configuration file
curl -L https://gitlab.com/brixly/file-dump/-/raw/master/filebeat.yml --output filebeat.yml

# Replace the default configuration file with the new one
sudo mv /etc/filebeat/filebeat.yml /etc/filebeat/filebeat.yml.bak
sudo mv filebeat.yml /etc/filebeat/filebeat.yml

# Restart the Filebeat service
sudo systemctl restart filebeat
