#!/usr/bin/python3

import json
import subprocess
import sys

# Check that there's an argument being passed, make it lowercase if so
if len(sys.argv) == 2:
	domain=str(sys.argv[1]).lower()
	if "www." in domain:
		# If www. is included in the domain strip it out
		domain=".".join(f"{domain}".split("www.")[1:])
else:
	sys.exit("Please provide a domain name as an argument!")

# Get the control panel in use
panel=subprocess.getoutput("cldetect --detect-cp-nameonly")

# If it's cPanel do the following
if panel == "cPanel":
	# Get data on the domain from a whmapi1 call
	data=json.loads(subprocess.getoutput(f"whmapi1 domainuserdata domain={domain} --output=json"))

	if data['metadata']['result'] == 0:
		# If the call fails report back the reason
		sys.exit(f"Failed! Error was {data['metadata']['reason']}")

	else:
		# Print the docroot if it succeeds
		print(data['data']['userdata']['documentroot'])

# If the panel id DirectAdmin
elif panel == "DirectAdmin":

	# Get information on the owner by calling whoowns
	whoowns=(subprocess.run(["/scripts/whoowns", f"{domain}", "--subdomain"], stdout=subprocess.PIPE))

	# If whoowns fails exit with error
	if whoowns.returncode == 1:
		sys.exit(f"Failed!")

	else:
		# If it suceeds decode the output from byte object
		output=whoowns.stdout.decode("utf-8")

		if 'subdomain' in output:
			# Split output to get owner
			owner=output.split()[0]
			# Since we know it's a subdomain, split the subdomain off of the main domain
			fullDomain=".".join(f"{domain}".split(".")[1:])
			subDomain=f"{domain}".split(".")[0]
			# Access DA API to get the docroot back
			print(json.loads(subprocess.getoutput(f'curl -s "$(/usr/local/directadmin/directadmin --root-auth-url-for=admin)/CMD_API_DOMAIN?action=document_root_all"'))['users'][f'{owner}']['domains'][f'{fullDomain}']['subdomains'][f'{subDomain}']['private_html'])

		else:
			# Split to remove \n character
			owner=output.split("\n")[0]
			print(json.loads(subprocess.getoutput(f'curl -s "$(/usr/local/directadmin/directadmin --root-auth-url-for=admin)/CMD_API_DOMAIN?action=document_root_all"'))['users'][f'{owner}']['domains'][f'{domain}']['private_html'])  
else:
	# Only supports DirectAdmin and cPanel currently
	sys.exit("Panel not supported.")