#!/bin/bash

cd /root
wget https://afterlogic.com/download/webmail-panel-installer.tar.gz
cd ./webmail-panel-installer
chmod a+x ./installer
./installer -a remove

cd /root
wget https://afterlogic.com/download/webmail-8-cpanel-installer.zip
cd ~
unzip ./webmail-8-cpanel-installer.zip -d webmail-8-cpanel-installer
cd ./webmail-8-cpanel-installer
chmod a+x ./installer
./installer -t lite -a install