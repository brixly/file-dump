<?php
/*
*
* Auto Accept Orders
* Created By Dennis Nind
*
*
* Hook version 1.0.0
*
**/
if (!defined("WHMCS"))
	die("This file cannot be accessed directly");

/*********************
 Auto Accept Orders Settings
*********************/
function brixlyAutoAcceptOrders_settings()
{
	return array(
		'apiuser'		=> '', // one of the admins username
		'autosetup' 		=> true, // determines whether product provisioning is performed
		'sendregistrar' 	=> true, // determines whether domain automation is performed
		'sendemail' 		=> true, // sets if welcome emails for products and registration confirmation emails for domains should be sent 
		'ispaid'		=> true, // set to true if you want to accept only paid orders
		'paymentmethod'		=> array(), // set the payment method you want to accept automatically (leave empty to use all payment methods) * example array('paypal','amazonsimplepay')
	);
}
/********************/

function brixlyAutoAcceptOrders_accept($vars) 
{
	$settings = brixlyAutoAcceptOrders_settings();

	$ispaid = true;

    $client_id = null;

	if($vars['InvoiceID'])
	{
		$result = localAPI('getinvoice', array(
			'invoiceid' 		=> $vars['InvoiceID'],
		), $settings['apiuser']);

		$ispaid = ($result['result'] == 'success' && $result['balance'] <= 0 && $result['status'] == 'Paid') ? true : false;

        $client_id = $result['userid'];
	}

	if((!sizeof($settings['paymentmethod']) || sizeof($settings['paymentmethod']) && in_array($vars['PaymentMethod'], $settings['paymentmethod'])) && (!$settings['ispaid'] || $settings['ispaid'] && $ispaid))
	{
		$result = localAPI('acceptorder', array(
			'orderid' 		=> $vars['OrderID'],
			'autosetup' 		=> $settings['autosetup'],
			'sendregistrar' 	=> $settings['sendregistrar'],
			'sendemail' 		=> $settings['sendemail'],
		), $settings['apiuser']);
	}

    // Log to the user via the WHMCS API
    $log_message = localAPI('LogActivity', array(
        'clientid'  => $client_id,
        'description' => "Order ID: " . $vars['OrderID'] . " Automatically Accepted via Brixly Hook",
    ));

}

add_hook('AfterShoppingCartCheckout', 0, 'brixlyAutoAcceptOrders_accept');

?>
