#!/bin/bash

EXIM_ENTRIES=$(tail -n1000 /var/log/exim_mainlog | grep "No Such User Here" | grep -v spamexperts | wc -l)

if test $EXIM_ENTRIES -ge 900; then
    echo "2 Backscatter_Attack - Detected $EXIM_ENTRIES No Such User entries in the last 1000 exim logs"
else
    echo "0 Backscatter_Attack - Nothing Detected"
fi